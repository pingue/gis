# GIS Mapper

Mapper application allowing for some funky things.

## Project setup
```
npm ci
composer install
```

## Compile the frontend application
```
npm run build
```

## Lints and fixes files
```
npm run lint
```

## Usage

## Docker
To run the server in a docker container:

`docker build -t gismapper .`

`docker run --rm -it -p <publicport>:80 --env-file .env gismapper`

For HTTPS support, use a separate NGINX proxy with SSL support to forward requests onto the docker container on the port provided above. 

