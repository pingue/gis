import Coordinates from "coordinate-parser";
import GeocodeClient from "./geocode_service_client";


function isValidLatLng(position) {
    var error;
    var isValid;
    try {
        isValid = true;
        new Coordinates(position);
        return isValid;
    } catch (error) {
        isValid = false;
        return isValid;
    }
}


function address_to_lnglat(content) {

    // Need to add this into a request to a server.
    if (content) {
        return GeocodeClient.resolve({ type: 'address', content});
    } else {
        return null;
    }


}

function pluscode_to_lnglat(content) {
    try {
        if (content) {
            var OpenLocationCode = require('open-location-code').OpenLocationCode;
            var openLocationCode = new OpenLocationCode();

            var globalTest = /(^|\s)([23456789C][23456789CFGHJMPQRV][23456789CFGHJMPQRVWX]{6}\+[23456789CFGHJMPQRVWX]{2,3})(\s|$)/i;
            if (!globalTest.test(content)) {
                // A local code was provided so we have to interpret a locality
                var localCode = /(?<first>^|^.*\s)(?<code>[23456789CFGHJMPQRVWX]{4,6}\+[23456789CFGHJMPQRVWX]{2,3})(?<last>\s.*$|$)/i;
                let split = localCode.exec(content)
                let localityString = (split["groups"]["first"] + split["groups"]["last"]).trim();

                return address_to_lnglat(localityString).then((locality) => {
                    console.log("resolved locality. doing second lookup");
                    let fullcode = openLocationCode.recoverNearest(split["groups"]["code"], locality[1], locality[0]);
                    const loc = openLocationCode.decode(fullcode);
                    return {source: content, fullcode: fullcode, ll: [loc["longitudeCenter"], loc["latitudeCenter"]]};
                })

            }

            const loc = openLocationCode.decode(content)
            return {source: content, ll:[loc["longitudeCenter"], loc["latitudeCenter"]]};

        } else {
            return null;
        }
    } catch (e) {
        console.log("Failed to parse '" + content + "' as pluscode: " + e);
        return null;
    }

}

function w3w_to_lnglat(content) {

    if (content) {
        return GeocodeClient.resolve({ type: 'w3w', content});
    } else {
        return null;
    }

}

function latlng_to_lnglat(content) {
    let c = null;
    try {
        if (content != null && content !== "") {
            c = new Coordinates(content);
            return {source: content, ll: [c.getLongitude(), c.getLatitude()]};
        } else {
            return null;
        }
    } catch (e) {
        console.log("Failed to parse '" + content + "' as latlon");
        return null;
    }
}

function magic_parse_location(content) {
    if (content == null || content == "") {
        return;
    }

    var regExpW3W = /^(\/\/\/)?[a-zA-Z]*\.[a-zA-Z]*\.[a-zA-Z]*$/;
    if (regExpW3W.test(content)) {
        return w3w_to_lnglat(content)
    }

    var regExpGlobalPlusCode = /(^|\s)([23456789C][23456789CFGHJMPQRV][23456789CFGHJMPQRVWX]{6}\+[23456789CFGHJMPQRVWX]{2,3})(\s|$)/i;
    var regExpLocalPlusCode = /(^|\s)([23456789CFGHJMPQRVWX]{4,6}\+[23456789CFGHJMPQRVWX]{2,3})(\s|$)/i;

    if (regExpGlobalPlusCode.test(content) || regExpLocalPlusCode.test(content)) {
        return pluscode_to_lnglat(content)
    }

    if (isValidLatLng(content)) {
        return latlng_to_lnglat(content)
    }
    console.log("failed to parse: " + content + "... trying as freestring address location")

    // TODO: Try the
    return address_to_lnglat(content)

}

export {w3w_to_lnglat, magic_parse_location, latlng_to_lnglat, pluscode_to_lnglat, address_to_lnglat}
