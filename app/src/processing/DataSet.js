export class DataSet
{

    constructor(datasource) {
        this.datasource = datasource;
        this.items = [];
        this.elements = []; // Per-dataset elements to be rendered
        this.errors = [];
        this.executedsteps = [];

        this.columns = new Map();
    }

    toGeoJSON() {

        let geojson = {}
        geojson.type = "FeatureCollection";
        geojson.features = [];


        this.items.forEach((item) => {
            item.elements.forEach((el) => {
                geojson.features.push(el);
            })
        })

        this.elements.forEach((el) => {
            geojson.features.push(el);
        });


        return geojson;
    }

}

export class DataSetItem {

    constructor(data) {

        this.elements = []; // Per-item elements to be rendered
        this.data = data; // The main data object/array/item
        this.source = data; // The original source line
        this.errors = [];
    }
}

export class DataSetExecutedStep {

    constructor(stepkey) {
        this.stepkey = stepkey;
        this.info = [];
        this.errors = [];
        this.hascompleted = true;
        this.haswarning = false;
        this.haserror = false;

    }

    static witherror(stepkey, error) {
        let exec_step = new DataSetExecutedStep(stepkey);
        exec_step.errors.push(error)
        return exec_step;
    }

    static withinfo(stepkey, error) {
        let exec_step = new DataSetExecutedStep(stepkey);
        exec_step.info.push(error);
        return exec_step;
    }

}