import {DataSetExecutedStep, DataSetItem} from "./DataSet";
import {w3w_to_lnglat, magic_parse_location, pluscode_to_lnglat, latlng_to_lnglat, address_to_lnglat} from "./location.js"
import _ from 'lodash';
import { parse as csvparse } from "csv-parse/lib/sync";
import Qty from 'js-quantities';
import GeocodeClient from "./geocode_service_client";


function evaluateAttribute(attribute, datacontext, defaultvalue = null) {
    // TODO: Handle different cases of literal values vs column values

    if(datacontext == null) return attribute ?? defaultvalue;

    // If we're pulling a value from another attribute/column, then we prefix
    // the value with an underscore. Else we treat it as a literal value.
    // _position = take the value from an attribute called 'position', else return defaultvalue if it's not there.

    if(typeof attribute === "string" && attribute.charAt(0) === "_") {

        if (datacontext.has(attribute.substring(1))) {
            return datacontext.get(attribute.substring(1));
        }
        return defaultvalue;
    }

    // Else return the attribute passed, or if the attribute was null, then returns the default value.
    return attribute ?? defaultvalue;



}

// Maps common attributes for rendering elements from a given data context and attribute values.
function mapElementAttributes(mapping, datacontext) {

    return {
        color: evaluateAttribute(mapping.color, datacontext, 'black'),
        fillColor: evaluateAttribute(mapping.fill, datacontext, 'red'),
        name: evaluateAttribute(mapping.name, datacontext, 'Marker'),
        weight: evaluateAttribute(mapping.stroke, datacontext, 3),
    }

}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


export const processors = {

    processors: {
        texttolines: function(dataset, step) {


            if(dataset.datasource.type !== "text") {
                dataset.executedsteps[step.id] = DataSetExecutedStep.witherror(step.id, "Data type not correct for this flow" );
                return null;
            }

            let items = dataset.datasource.text.split(step.newline);

            items = items.map(i => i.trim());
            items = items.filter(i => i.length);

            // Create a new item for each row.
            items = items.map(i => {
                return new DataSetItem(i);
            });

            dataset.items = items;
            dataset.executedsteps[step.id] = DataSetExecutedStep.withinfo(step.id, "Created " + items.length + " items");


        },

        split: function(dataset, step) {

            if(!step.format) return null;

            let splitters = new Map([
                ['whitespace', (d) => d.split(RegExp('\\s+'))], // one or more whitespace.
                ['comma', (d) => d.split(RegExp(","))],
                ['custom', (d) => d.split(RegExp(step.customformatter ?? ""))],
                ['csv', (d) => csvparse(d)[0] ],
            ]);

            // Convert a single input line into the individual parts used
            dataset.items.map((dsi) => {
                if(typeof dsi.data !== "string") return dsi;

                if(!splitters.has(step.format)) console.error("No splitter found for", step.format);

                dsi.data = splitters.get(step.format)(dsi.data);
                dsi.data = dsi.data.map(e => e.trim());
            });


        },

        parse: async function(dataset, step) {

            if(!step.mappings) return null;

            for(let dsi of dataset.items) {
                let mappedcolumns = new Map();

                for(let mapping of step.mappings) {
                    let content = mapping.column.map((columnid) => {
                        return dsi.data[columnid] ?? '';
                    });

                    content = content.join(" ").trim();

                    // Do the parsing here of type (content will always be string here)
                    if(mapping.type === "latlon"){
                        content = latlng_to_lnglat(content)
                    } else if(mapping.type === "pluscode"){
                        content = pluscode_to_lnglat(content);
                    } else if(mapping.type === "w3w"){
                        content = w3w_to_lnglat(content);
                    } else if(mapping.type === "address"){
                        content = address_to_lnglat(content);
                    } else if(mapping.type === "location"){
                        content = magic_parse_location(content);
                    } else if(mapping.type === "number"){
                        content = parseFloat(content);
                    } else if(mapping.type === "distance"){
                        let q = Qty(content);

                        if(q.isUnitless()) q = Qty(content + " " + (mapping.defaultunit ?? "m")); // assume metres

                        if(q.kind() !== "length") {
                            dsi.errors.push("Content of '" + content + "' could not be parsed as a length");
                            return;
                        }

                        content = q.to("m").scalar;
                        if(!content) {
                            debugger;
                            return;
                        }

                    }


                    if(content instanceof Promise) {

                        console.log("Setting mapping", mapping.name, "with promise resolved from", content);
                        content.then((content) => {
                            console.log("Setting mapping", mapping.name, "from returned promise", content);
                            mappedcolumns.set(mapping.name, content);
                        })
                    } else {
                        console.log("Setting mapping", mapping.name, "directly with", content);
                        mappedcolumns.set(mapping.name, content);
                    }
                    dataset.columns.set(mapping.name, mapping);


                }

                dsi.data = mappedcolumns;

            }

            console.log("Awaiting geocode");
            await GeocodeClient.execute();
            console.log("Geocoded!");

        },

        marker: function(dataset, step) {

            if(!step.position) return null;

            dataset.items.map((dsi) => {


                let pos = evaluateAttribute(step.position, dsi.data);
                if(!pos) return;

                dsi.elements.push({
                    type: 'Feature',
                    properties: mapElementAttributes(step, dsi.data),
                    geometry: {
                        type: "Point",
                        coordinates: pos.ll,
                    }
                })
            });

        },

        circle: function(dataset, step) {

            if(!step.position) return null;

            dataset.items.map((dsi) => {
                let pos = evaluateAttribute(step.position, dsi.data);
                if(!pos) return;

                dsi.elements.push({
                    type: 'Feature',
                    properties: {
                        shape: "circle",
                        radius: evaluateAttribute(step.radius, dsi.data, 1),
                        ...mapElementAttributes(step, dsi.data)
                    },
                    geometry: {
                        type: "Point",
                        coordinates: pos.ll,
                    }
                })
            });

        },


        polyline: function(dataset, step) {

            if(step.positions) { // One polyline for each item in the dataset.

                dataset.items.map((dsi) => {

                    let linepoints = [];

                    step.positions.forEach((position) => {
                        let pos = evaluateAttribute(position, dsi.data);
                        if(!pos) return;
                        linepoints.push(pos.ll);
                    });



                    dsi.elements.push({
                        type: 'Feature',
                        properties: mapElementAttributes(step, dsi.data),
                        geometry: {
                            type: "LineString",
                            coordinates: linepoints,
                        }
                    })
                });

            } else if(step.position) { // One polyline for the whole dataset.

                let linepoints = [];

                dataset.items.map((dsi) => {
                    let pos = evaluateAttribute(step.position, dsi.data);
                    if(!pos) return;
                    linepoints.push(pos.ll);
                });

                dataset.elements.push({
                    type: 'Feature',
                    properties: mapElementAttributes(step, null),
                    geometry: {
                        type: "LineString",
                        coordinates: linepoints,
                    }
                });
            }

        }


    },

    async process(dataset, step) {
        console.log("Processing " + step.type);

        if(!this.processors[step.type]) {
            console.error("Failed to process step", step);
            return;
        }

        await this.processors[step.type](dataset, step);
        console.log("Processing " + step.type + " done");
    },









}
