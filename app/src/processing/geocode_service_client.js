import hash from "object-hash";

/**
 * This class handles batch processing of geocode style lookups
 * This will take requests in the form of calls to "resolve" which will pass a type and content to the geocoding backend
 * and will return a Promise object for when the lookup has completed.
 * When the "execute" method is called, a singular request to the geocoding backend will be made which in turn
 * will process the various requests behind the scenes, or load them from cache. A single object will be returned
 * with all the items resolved. The given promises to the callers of the "resolve" functions will then be resolved.
 */
class GeocodeServiceClient {

    constructor() {
        this.promises = new Map();
    }

    /**
     *
     * @param query
     * @returns {Promise<unknown>}
     */
    resolve(query) {
        // TODO: Double check the cache here?

        let query_hash = hash(query);

        let resolver = null;
        let p = new Promise((res) => { resolver = res; });
        this.promises.set(query_hash, {query, promise: p, resolver: resolver});
        return p;

    }

    /**
     * Executes the set of coordinates as a single request to the geocoder service rather than one request per-entry
     * This then resolves each individual promise in return when it gets a successful answer
     * @returns {Promise<void>}
     */
    async execute() {

        // Do a request to the geocoder microservice here.

        let queries = Array.from(this.promises, ([key,value]) => { return {key, query: value.query}});

        if(queries.length === 0) {
            console.log("No queries to make");
            return;
        }

        console.log("Doing request with", queries)
        let response = await fetch("http://localhost:3000/resolve", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ queries })
        });

        let responsejson = await response.json();

        // We should have an array of replies here.
        if(!responsejson.replies) {
            debugger;
        }

        for(let responseitem of responsejson.replies){
            // Use the key/hash to look up the relevant promise.
            let pr = this.promises.get(responseitem.key);
            pr.resolver(responseitem.response);
        }

        this.promises.clear();

    }


}

const GeocodeClient = new GeocodeServiceClient();
export default GeocodeClient;