import DatatableItem from "./DatatableItem";
import DatatableItemlocation from "./DatatableItemlocation";
import DatatableItemtext from "./DatatableItemtext";


export default {
    DatatableItem,
    DatatableItemlocation,
    DatatableItemtext,
}