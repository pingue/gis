import { createApp } from 'vue'
import App from "./components/App.vue";
import moment from 'moment';
import { store } from './store';
import './index.css'

const app = createApp(App);

app.use(store);

/* Vue 3 doesn't support filters, so this is a cheeky little workaround for the time being */
/* https://v3.vuejs.org/guide/migration/filters.html#migration-strategy */
app.config.globalProperties.$filters = {
  formatDateHuman(value) {
    if (value) {
      let m = moment(String(value));

      if (m.diff(moment.now(), 'days') < -7) {
        return m.fromNow();
      }
    }
  },
  formatDate(value) {
    if (value) {
      let m = moment(String(value));
      return m.format("Do MMM");

    }
  }
};

app.mount('#app');

