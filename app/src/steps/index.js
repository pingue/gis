import FlowStepcircle from "@/steps/FlowStepcircle";
import FlowStepmarker from "@/steps/FlowStepmarker";
import FlowStepparse from "@/steps/FlowStepparse";
import FlowSteppolyline from "@/steps/FlowSteppolyline";
import FlowStepsplit from "@/steps/FlowStepsplit";
import FlowSteptexttolines from "@/steps/FlowSteptexttolines";
import FlowStep from "@/steps/FlowStep";



export default {
    FlowStep,
    FlowStepcircle,
    FlowStepmarker,
    FlowStepparse,
    FlowSteppolyline,
    FlowStepsplit,
    FlowSteptexttolines,
}