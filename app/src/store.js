import {createStore} from 'vuex';
import {processors} from "./processing/processors";
import {DataSet, DataSetExecutedStep} from "@/processing/DataSet";


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


export const store = createStore({

    state() {
        return {
            datasourceid: '',
            flowid: '',
            user: null,
            loadingstate: true,
            savingstate: false,
            dataset: null,
            datasources: [
                {
                    name: "Data Conversion",
                    id: 'dconv',
                    type: 'text',
                    text: `Annex,SO18 5TS
                            Soton,Southampton UK
                            Postcode,SO17 1BJ 
                            Soton Airport,"Airport near [50.9, -1.404]"`,

                },
                {
                    name: "CSV Set",
                    id: 'csv',
                    type: 'text',
                    text: `#> Aruba,       -69.97345,  12.51678, 300 miles,  12° 26′ 46″ S 40° 58′ 56″ W ,   37° 26.767′ N 139° 58.933′ W
                            #> Afghanistan,  66.00845,  33.83627, 380 km,  12° 26′ 46″ N 29° 58′ 56″ E,
                            #> Annex,       17.53646, -12.29118 , 340000yards,  ///icons.buddy.solar, 
                            #> Anguilla,    -63.06082,  18.22560, 190 nmi, Southampton UK
                            #> Albania,      20.05399,  41.14258, 240 mi, 
                            #> Aland,        20.03715,  60.20733, 290, `,

                },
                {
                    name: "Master Set",
                    id: 'master',
                    type: 'text',
                    text: `#> Aruba       -69.97345  12.51678   40° 26′ 46″ N 79° 58′ 56″ W    46° 26.767′ N 79° 58.933′ W\n
                #> Afghanistan  66.00845  33.83627   43° 26′ 46″ N 79° 58′ 56″ W\n
                #> Angola       17.53646 -12.29118   44° 26′ 46″ N 79° 58′ 56″ W\n
                #> Anguilla    -63.06082  18.22560\n
                #> Albania      20.05399  41.14258\n
                #> Aland        20.03715  60.20733\n`,

                },
            ],
            flows: [
                {
                    name: "Data conversion testing",
                    id: "dconv",
                    steps: [
                        {id: 1, type: 'texttolines', newline: '\n'},
                        {id: 2, type: 'split', format: 'csv'},
                        {
                            id: 3,
                            type: 'parse',
                            mappings: [
                                {name: 'title', column: [0], type: 'text'},
                                {name: 'position', column: [1], type: 'location', displayas: 'osgridref'},
                            ]
                        },

                        {id: 4, type: 'marker', position: '_position', name: '_title'},

                    ],
                },
                {
                    name: "CSV parse",
                    id: "csv",
                    steps: [
                        {id: 1, type: 'texttolines', newline: '\n'},
                        {id: 2, type: 'split', format: 'csv'},
                        {
                            id: 3,
                            type: 'parse',
                            mappings: [
                                {name: 'title', column: [0], type: 'text'},
                                {name: 'position', column: [2, 1], type: 'location'},
                                {name: 'position1', column: [4], type: 'location'},
                                {name: 'position2', column: [5], type: 'location'},
                                {name: 'rad', column: [3], type: 'distance', defaultunit: 'km'},
                            ]
                        },

                        {id: 4, type: 'marker', position: '_position', name: '_title'},
                        {id: 5, type: 'marker', position: '_position1', name: '_title'},
                        {id: 6, type: 'marker', position: '_position2', name: '_title'},
                        {id: 7, type: 'polyline', positions: ['_position', '_position1', '_position2'], color: 'red'},
                        {id: 8, type: 'polyline', position: '_position', color: 'limegreen'},
                        {id: 9, type: 'circle', position: '_position', radius: 800000, stroke: 2, color: 'orange'},
                        {id: 10, type: 'circle', position: '_position', radius: '_rad', stroke: 1, color: 'red'}

                        // Sort?

                        // Table view

                        // Input types

                    ],
                },
                {
                    name: "blah",
                    id: "master",
                    steps: [
                        {type: 'texttolines', newline: '\n'},
                        {type: 'split', format: 'whitespace'},
                        {
                            type: 'parse', mappings: [
                                {name: 'title', column: [1], type: 'text'},
                                {name: 'position', column: [2, 3], type: 'latlon_decimal'},
                                {name: 'position1', column: [4, 5, 6, 7, 8, 9, 10, 11], type: 'latlon_dms'},
                                {name: 'position2', column: [12, 13, 14, 15, 16, 17], type: 'latlon_degrees'}
                            ]
                        }
                    ],
                }
            ]

        };
    },

    mutations: {
        setLoadingState(state, isloading) {
            state.loadingstate = isloading;
        },

        setSavingState(state, issaving) {
            state.savingstate = issaving;
        },

        setProcessedDataset(state, dataset) {
            state.dataset = null;
            console.log("Setting dataset from", state.dataset, dataset);
            state.dataset = dataset;
        },

        setDatasourceFlowId(state, ids) {
            state.datasourceid = ids.datasource;
            state.flowid = ids.flow;
        },

        updateFlowSteps(state, params) {
            let flow = state.flows.find((flow) => flow.id === params.flowid);
            if (!flow) return;

            flow.steps = params.steps;
        },
    },

    actions: {
        async loadInitialData({commit}) {
            commit('setLoadingState', true);
            commit('setLoadingState', false);
        },

        async setDatasourceFlow({dispatch, commit}, ids) {
            commit('setDatasourceFlowId', ids);
            await dispatch('processData');
        },


        async processData({commit, getters}) {
            commit('setLoadingState', true);

            let ds = getters.getDatasource;
            // Todo: look up the appropriate flow for the dataset.
            let flow = getters.getFlow;
            console.log("Processing", ds.name, "with flow", flow.name);

            ds = new DataSet(ds);

            for (let step of flow.steps) {
                try {
                    ds.executedsteps[step.id] = DataSetExecutedStep.withinfo(step.id, "Processing...");
                    await processors.process(ds, step);

                    await sleep(Math.random() * 60);
                    ds.executedsteps[step.id] = DataSetExecutedStep.withinfo(step.id, "Completed");


                    commit('setProcessedDataset', ds);
                } catch (e) {
                    ds.executedsteps[step.id] = DataSetExecutedStep.witherror(step.id, "Whilst processing step" + step.id + ": " + e);
                }

            }

            commit('setProcessedDataset', ds);

            commit('setLoadingState', false);

            return ds;


        }
    },

    getters: {


        getDatasource: (state, getters) => {
            return getters.getDatasetById(state.datasourceid);
        },
        getFlow: (state, getters) => {
            return getters.getFlowById(state.flowid);
        },

        getDatasetById: (state) => (key) => state.datasources.find((ds) => ds.id === key),
        getFlowById: (state) => (key) => state.flows.find((flow) => flow.id === key),

        getProcessedDataset: (state, getters) => {
            return state.dataset;
        },


    }

});

