require('console-stamp')(console);
require('dotenv').config();
var express = require("express");
var app = express();
var cors = require('cors');

const NodeCache = require("node-cache");
const lookupCache = new NodeCache();

app.use(express.json());
app.use(cors({origin: process.env.GISMAPPER_URL}));

app.post("/resolve", async (req, res, next) => {

    if (!req.body.queries) throw new Error("no queries passed");

    let reply = {replies: []};

    for (let entry of req.body.queries) {

        let location = null;

        if (lookupCache.has(entry.key)) {
            location = lookupCache.get(entry.key);
            console.log("Found cached entry for", entry.key, "at", location);

            reply.replies.push({
                key: entry.key,
                response: location,
            })

        } else {
            console.log("Looking up key", entry.key, "with", JSON.stringify(entry.query));
            if (entry.query.type === "address") {
                await address_lookup(entry.query.content).then((ll) => {
                    location = ll;
                })
            } else if (entry.query.type === "w3w") {
                await w3w_lookup(entry.query.content).then((ll) => {
                    location = ll;
                })
            }

            if (location) {
                console.log("Caching", entry.key, "to", location);
                lookupCache.set(entry.key, location);
                reply.replies.push({
                    key: entry.key,
                    response: location,
                })
            } else {
                console.log("Failed to look up key", entry.key, "with", JSON.stringify(entry.query));
            }
        }


    }

    res.json(reply);
});


app.listen(3000, () => {
    console.log("Server running on port 3000");
});

let NodeGeocoder = require('node-geocoder');
const nodeFetch = require('node-fetch');
const geocoder = NodeGeocoder({
    provider: 'openstreetmap',
    fetch: function fetch(url, options) {
        return nodeFetch(url, {
            ...options,
            headers: {
                'user-agent': 'GISMapper <gitlab/pingue/gis>',
            }
        });
    }
});

async function address_lookup(address) {
    let addresses = [];
    try {
        addresses = await geocoder.geocode({
            q: address,
            extratags: 1,
            namedetails: 1,
            limit: 1,
        });
    } catch (e) {
        console.log("Error in geocoding", e);
        return null;
    }

    console.log(addresses);

    // The lookup _can_ return multiple things from a search, so we take the first as that's the most "common" result.
    if (addresses.length >= 0) {
        return { source: address, ll: [addresses[0].longitude, addresses[0].latitude]};
    }

    return null;

}


const what3words = require("@what3words/api");

function w3w_lookup(content) {
    const apiKey = process.env.W3W_API_KEY;
    const config = {
        host: process.env.W3W_API_HOST,
        apiVersion: 'v3',
    }
    let w3wService = what3words.ConvertToCoordinatesClient.init(apiKey, config);
    let promise = w3wService.run({words: content})
    return promise.then((response) => {
        return { source: content, ll: [response.coordinates.lng, response.coordinates.lat]};
    });
}