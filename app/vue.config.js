module.exports = {
    filenameHashing: false,
    chainWebpack: config => {
        config.plugins.delete('html');
        config.plugins.delete('preload');
        config.plugins.delete('prefetch');
    },
    runtimeCompiler: true,
    lintOnSave: false,
    configureWebpack: (config) => {
        config.devtool = 'source-map'
    },
    outputDir: 'serve/dist',


    pages: {
      index: {
          entry: 'src/main.js',
          template: 'serve/index.htm',
          filename: 'index.htm',
          title: 'GISMapper',
      }
    },

    devServer: {
        clientLogLevel: 'debug',
        port: 9000,
    }
}