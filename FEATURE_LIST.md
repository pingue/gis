For a point on a map, show:
Lat/lng (decimal and minutes/seconds), northing/easting, w3w, Googlelocationcodething, address, nearest point of interest, show satellite view, show map view, nearest airport because why not, town name (as list)

Map tools:
* Measure distance
* plot points
* plot circle around point
* draw line between points
* measure angle between 3 points
* draw angles

Input methods : 
* Click on map
* Text based input (CSV / TSV)
* Lat/lng (`12.12 S`, `-12.12`, `12° 12.12`, `12° 12' 12"`)
* OS Grid ref (of different lengths)
* What3Words
* Google Plus Code (full and localised)
* Encoded polyline
* Place names / Addresses / PoI lookup

fancy features:
* Processing pipelines
  * `<text CSV input> --> <parse per row as 'lat,lng,color,name' or 'w3w,title' or google code, n/e, OS grid ref... > --> <loop through the set of items and add lines in a Delaunay triangulation for it> ---> <loop through each item and add a circle radius "<title>">  ---> <plot on google map> `
  * `<text csv input> ----> <read each row as lat/lon/lat/lon/lat/lon > ---> <plot each triangle as polyline / closed polyline > ---> < plot on openstreetmap > `
  * `<blank input array I can click things into from a map> --> <draw a distance matrix> `
  * `---> (foreach item, geolookup the nearest town and print a list ) ---> `
